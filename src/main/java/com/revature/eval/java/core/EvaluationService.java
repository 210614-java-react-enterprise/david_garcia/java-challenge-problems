package com.revature.eval.java.core;


import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class EvaluationService {

	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param string
	 * @return
	 */
	public String reverse(String string) {

		System.out.println("Running string reversal method");                   // This prints to ensure the method is running
		System.out.println("This is your vanilla string: " + string);                 // This prints the unreversed string, for comparison with reversed string

		char[] stringArray = string.toCharArray();                // char array created called stringArray which converts the pushed in string into an array containing all the individual characters of the string
		int stringCount = stringArray.length;                // integer counting the number of characters in the given string
		int reverseStringCount = 0;                  // integer setting up the array pointer for our reversed string's array
//        System.out.println("stringCount is: " + stringCount);
		char[] reverseStringArray = new char[stringCount];                  // creating a new char array called reverse string which contains the same number of characters as the original array due to integer stringCount

		while(stringCount != -1){                // while loop is true until the break command runs and stops the loop
			stringCount -= 1;                 // stringCount integer is subtracted by one, this is because it is tracking the length of the original array, and in order to assign the proper values, I need it to properly track to the array
			if (stringCount == -1) break;                // Once the loop reaches value -1, which is invalid for an array index, the loop will break - note- this didn't work correctly when I had it in the while loop arguments so I put it in the actual body
			reverseStringArray[reverseStringCount] = stringArray[stringCount];                 // the reverse string array starts creating its index at integer 0 due to reverseStringCount, but takes the value from the end of the given array due to stringCount tracking from the end of its index
			reverseStringCount += 1;                 // Adds 1 to reverseStringCount in order to move up once in the index
//            System.out.println(reverseStringArray);
		}

		String reverse = String.valueOf(reverseStringArray);               // This converts the char array "reverseStringArray" into a string "reverseString"
		System.out.println("This is your reversed string: " + reverse);                 // This prints the reversed string - ensures code is working
		return reverse;                // Returns "reverseString"
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 */
	public String acronym(String phrase) {
		// TODO Write an implementation for this method declaration
		//        String phrase = "Graphical User Interface";
		System.out.println("Running Acronym method");
		System.out.println("Your Phrase is: " + phrase);
		String[] words = phrase.split("-|\\s"); //splits the string based on spaces and if it has a "-"
		char[] acronymArray = new char[words.length]; // Creates an array which will contain the characters taken from the string array

		for(int count = 0; count <= words.length -1;count++){ // A for loop which uses the count int to compare against the length of the word array minus 1, once it loops it adds one to the count
			acronymArray[count] = words[count].charAt(0); // Populates the acronym char array at index pointer decided by the count variable, with the char taken from the string of the same index pointer in the word array, char 0 indicates it is the first character being pulled
		}
		String acronym = String.valueOf(acronymArray); // converts the acronym char array into a string
		System.out.println(acronym); // prints our acronym string (testing purposes)
		return acronym.toUpperCase(Locale.ROOT); // returns our acronym string to calling class

		//return null;
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle(){
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree){
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
			System.out.println(sideOne + " " + sideTwo + " " + sideThree);
			isEquilateral();
			System.out.println("isEquilateral: " + isEquilateral());

			//System.out.println("isIsosceles: " + isIsosceles(sideOne,sideTwo,sideThree));
			//System.out.println("isScalene: " + isScalene(sideOne,sideTwo,sideThree));
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral(){
			if(sideOne == sideTwo && sideOne == sideThree && sideTwo == sideThree){
				//System.out.println("Passing thru isEquilateral");
				return true;
			} else{
				//System.out.println("passing thru else");
				return false;
			}
		}

		public boolean isIsosceles(){
			if(sideOne == sideTwo || sideOne == sideThree || sideTwo == sideThree){
				return true;
			}else{return false;}
		}

		public boolean isScalene(){
			if(sideOne != sideTwo && sideOne != sideThree && sideTwo != sideThree){
				return true;
			} else{return false;}
		}
	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 * 
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 */
	public int getScrabbleScore(String string) {

		char[] pt1 = {'A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'};
		char[] pt2 = {'D', 'G'};
		char[] pt3 = {'B','C', 'M','P'};
		char[] pt4 = {'F', 'H', 'V', 'W','Y'};
		char[] pt5 = {'K'};
		char[] pt8 = {'J','X'};
		char[] pt10 = {'Q','Z'};
		String[] pointArray = {"pt1[i]","pt2[i]","pt3[i]","pt4[i]","pt5[i]","pt8[i]","pt10[i]"};

		int index = pt1.length;
		int index2 = pt2.length;
		int index3 = pt3.length;
		int index4 = pt4.length;
		int index5 = pt5.length;
		int index8 = pt8.length;
		int index10 = pt10.length;

		String upperString = string.toUpperCase(Locale.ROOT);
		//System.out.println(upperString);

		long score1 = 0;
		long score2 = 0;
		long score3 = 0;
		long score4 = 0;
		long score5 = 0;
		long score8 = 0;
		long score10 = 0;


		long points = 0;
		int i = 0;
		String variableChar = pointArray[0];

		for(i = 0; i<index; i++){
			char checkedChar1 = pt1[i];
			score1 = upperString.chars().filter(ch -> ch == checkedChar1).count();
			System.out.println(score1);
			points = score1 + points;
		}

		System.out.println("current points: " + points);

		for(i = 0; i<index2; i++) {
			char checkedChar2 = pt2[i];
			score2 = upperString.chars().filter(ch -> ch == checkedChar2).count();
			System.out.println("Score 2: " + score2);
			points = (score2 * 2) + points;
			System.out.println("points calculated after checking char2 " + points);
		}
		//points = score1 + points;
		System.out.println("current points: "+ points);
		for(i = 0; i<index3; i++) {
			char checkedChar3 = pt3[i];
			score3 = upperString.chars().filter(ch -> ch == checkedChar3).count();
			System.out.println("Score 3: " + score3);
			points = (score3 * 3) + points;
			System.out.println("points calculated after checking char3 " + points);
		}
		//points = score1 + points;
		System.out.println("current points: "+ points);

		for(i = 0; i<index4; i++) {
			char checkedChar4 = pt4[i];
			score4 = upperString.chars().filter(ch -> ch == checkedChar4).count();
			System.out.println("Score 4: " + score4);
			points = (score4 * 4) + points;
			System.out.println("points calculated after checking char4 " + points);
		}
		//points = score1 + points;
		System.out.println("current points: "+ points);

		for(i = 0; i<index5; i++) {
			char checkedChar5 = pt5[i];
			score5 = upperString.chars().filter(ch -> ch == checkedChar5).count();
			System.out.println("Score 5: " + score5);
			points = (score5 * 5) + points;
			System.out.println("points calculated after checking char5 " + points);
		}
		//points = score1 + points;
		System.out.println("current points: "+ points);
		for(i = 0; i<index8; i++) {
			char checkedChar8 = pt8[i];
			score8 = upperString.chars().filter(ch -> ch == checkedChar8).count();
			System.out.println("Score 8: " + score8);
			points = (score8 * 8) + points;
			System.out.println("points calculated after checking char8 " + points);
		}
		//points = score1 + points;
		System.out.println("current points: "+ points);
		for(i = 0; i<index10; i++) {
			char checkedChar10 = pt10[i];
			score10 = upperString.chars().filter(ch -> ch == checkedChar10).count();
			System.out.println("Score 10: " + score10);
			points = (score10 * 10) + points;
			System.out.println("points calculated after checking char10 " + points);
		}
		//points = score1 + points;
		System.out.println("current points: "+ points);

		int intPoints = Math.toIntExact(points);

		return intPoints;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 * 
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 * 
	 * The format is usually represented as
	 * 
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 * 
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 * 
	 * For example, the inputs
	 * 
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 * 
	 * 6139950253
	 * 
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {
		// TODO Write an implementation for this method declaration
		System.out.println(string);
		String[] splitArray = string.split("\\s|\\.|\\)|\\(|\\-");
		//System.out.println(splitArray.length);
		String clean = new String();

		for(String splitted : splitArray){
			//System.out.println(splitted);
			clean = clean.concat(splitted);
		}
		System.out.println(clean);
		if(clean.matches("[0-9]+")) {
			try {
				if (clean.length() <= 10) {
					System.out.println("inside if");
					return clean;
				} else {
					throw new IllegalArgumentException();
				}
			} catch (IllegalArgumentException e) {
				throw e;
			}
		}else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		// TODO Write an implementation for this method declaration
		Map<String, Integer> actualWordCount = new HashMap<>();
		System.out.println(string);

		string = string.replaceAll("[,\n]", " ");
		System.out.println(string);
		//string = string.replaceAll("\n", " ");
		//System.out.println(string);
		String[] splitArray = string.split("\\s|\\.|\\)|\\(|\\-");
		System.out.println(Arrays.toString(splitArray));
		String splitted = String.valueOf(splitArray[0]);
		//System.out.println(splitted);
		int v = 2;
		int nonEmpty = 0;

		for (String spaceCheck : splitArray){
			if (spaceCheck.length() >0){
				nonEmpty++;
			}
			System.out.println(spaceCheck.length());
		}
		String[] fixedArray = new String[nonEmpty];
		System.out.println("This is fixed array length " + fixedArray.length);
		System.out.println(Arrays.toString(splitArray));
		int i = 0;
		for (String spaceCheck: splitArray){
			System.out.println("Inside spaceCheck");
			if (spaceCheck.length() >0){
				System.out.println("inside If Spacecheck");

				fixedArray[i] = spaceCheck;
				System.out.println(fixedArray[i]);
				i++;
				System.out.println("i is: " + i);
			}
		}

		System.out.println(" Fixed Array? "+ Arrays.toString(fixedArray));

		for(i = 0; i< fixedArray.length; i++){
			String check = fixedArray[i];
			System.out.println(check);

			if(actualWordCount.containsKey(check)){
				actualWordCount.put(check,v++);
			} else{
				actualWordCount.put(check,1);
			}

		}

 /*       actualWordCount.put("one",1);
        actualWordCount.put("of",1);*/

		System.out.println(actualWordCount);

		return actualWordCount;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T extends Comparable> {
		private List<T> sortedList;

		public static void main(String[] args) {
		}

		public int indexOf(T t) {
			// TODO Write an implementation for this method declaration
			System.out.println("size is: " + sortedList.size());
			int half = (sortedList.size() / 2);
			System.out.println("half is: " + half);
			System.out.println(sortedList.toString());
			System.out.println(sortedList.get(half));
			int compResultFirstHalf = t.compareTo(sortedList.get(half));
			int i = 0;
			int lastHalf = ((half + sortedList.size()) / 2);
			//System.out.println("Last half is: " + lastHalf);
			int compResultLastHalf = t.compareTo(sortedList.get(lastHalf));
			//System.out.println("Last half result is: " + compResultLastHalf);
			System.out.println("First half result is: " + compResultFirstHalf);


			while (i <= 20) {
				if(i == 9) {
					half++;
				}
				if(i == 13)
				{
					half++;
				}
				System.out.println("i: "+ i);
				System.out.println("h: "+ half);
				System.out.println("fh: "+ compResultFirstHalf);
				//System.out.println("lh: "+ compResultLastHalf);
				compResultFirstHalf = t.compareTo(sortedList.get(half));

				switch (compResultFirstHalf) {
					case -1:
						half = half / 2;
						System.out.println("half is: " + half);
						compResultFirstHalf = t.compareTo(sortedList.get(half));
						System.out.println(compResultFirstHalf);
						i++;
						break;
					case 0:
						return half;
					case 1:
						System.out.println("top of case 1");
						System.out.println(sortedList.size());
						half = ((half + sortedList.size())/2);
						System.out.println("case 1 h: "+ half);
						compResultFirstHalf = t.compareTo(sortedList.get(half));
						i++;
						break;

				}
			}
			return 0;
		}



		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}


	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration

		//System.out.println(string);
		String[] stringSplit = string.split("\\s");
		char[] vowels = {'a','e','i','o','u'};
		//System.out.println(stringSplit.length);
		List<List<Character>> stringCharMultiArray = new ArrayList<List<Character>>();

		for (char c : vowels){ if (string.charAt(0) == c){ return (string+"ay"); } }

		String[] pigLatinSplit = new String[stringSplit.length];

		if(stringSplit.length>1){
			int ifCount = 0;

			for (String s : stringSplit){

				char[] stringArray = s.toCharArray();
				char[] pigLatinCharArray = new char[stringArray.length];

				int scopedInt = 0;

				if(s.charAt(0) == 'q') {
					scopedInt = 2;

					for (int r = 0; r < s.length(); r++){
						if (scopedInt == s.length()) { scopedInt = 0; }

						pigLatinCharArray[r] = stringArray[scopedInt];

						scopedInt++;
					}
				}

				else {
					for(int i = 0; i < s.length(); i++){
						for (char c : vowels) {
							if(c == s.charAt(i)){

								stringArray = s.toCharArray();
								pigLatinCharArray = new char[stringArray.length];

								for (int r = 0; r < s.length(); r++){

									if (i == s.length() ) { i = 0; }
									pigLatinCharArray[r] = stringArray[i];
									i++;

								}
							}
						}
					}

				}

				String pL = new String(pigLatinCharArray);
				System.out.println(pL);
				pigLatinSplit[ifCount] = pL;
				ifCount++;

				if (ifCount == stringSplit.length){
					StringBuffer sb = new StringBuffer();
					for(int i = 0; i < pigLatinSplit.length; i++){
						sb.append(pigLatinSplit[i]);
						sb.append("ay ");
					}
					String str = sb.toString();
					return str.trim();

				}
			}
		}

		for(int i = 0; i < string.length(); i++){
			for (char c : vowels) {
				if(c == string.charAt(i)){
					char[] stringArray = string.toCharArray();
					char[] pigLatinCharArray = new char[stringArray.length];

					for (int r = 0; r < string.length(); r++){

						if (i == string.length() ) { i = 0; }
						pigLatinCharArray[r] = stringArray[i];
						i++;

					}

					String pL = new String(pigLatinCharArray);
					return (pL+"ay");
				}
			}
		}


		return null;
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		double length;
		length = String.valueOf(input).length();
		System.out.println("length: " + length);
		double test;
		double value = 0;
		double inputDub = input;
		while (input > 0) {

			System.out.println( "input mod 10: " + input % 10);
			test = (input % 10);
			System.out.println("test: " + test);

			test = Math.pow(test, length);
			System.out.println("post math test: " + test);
			value += test;

			input = input / 10;
			test = (input % 10);

			System.out.println("input: " + input);
			System.out.println("test: " + test);

			System.out.println("value is " + value);

		}

		System.out.println(inputDub);
		System.out.println(value);
		System.out.println(value == inputDub);
		return value == inputDub;
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		// TODO Write an implementation for this method declaration
		List<Long> arrayList = new ArrayList<>();

		while (l % 2 == 0){
			System.out.println("test");
			l/= 2;
			arrayList.add(2L);
		}

		System.out.println("outside while");

		for(int n = 3; n<= Math.sqrt(l); n+= 2){
			while (l % n == 0){
				l /= n;
				System.out.println(l + " ");
				System.out.println("test");
				arrayList.add((long)n);
			}
		}

		if (l>2){
			System.out.println(l);
			arrayList.add(l);
		}

		System.out.println(arrayList);

		return arrayList;
	}

	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;
		LinkedList<Character> vanillaAlphabet = new LinkedList<>();
		LinkedList<Character> rotAlphabet = new LinkedList<>();

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String string) {
			// TODO Write an implementation for this method declaration

			for (char ch = 'A'; ch <= 'Z'; ++ch)
			{
				vanillaAlphabet.add(ch);
			}
			System.out.println(key);
			System.out.println(vanillaAlphabet);

			int rotation = 0;
			//this for loop creates the linkedlist for our rotated alphabet
			for (int i = key; i < vanillaAlphabet.size(); i++){
				if (i == key){
					rotation ++;
					//System.out.println("rotation is: " + rotation);
				}
				if (rotation == 2){
					break;
				}
				//System.out.println("i: " + i);
				//System.out.println(vanillaAlphabet.get(i));
				rotAlphabet.add(vanillaAlphabet.get(i));

				if (i == 25){
					i = -1;
				}

			}

			String[] splitArray = string.split("\\s|\\)|\\(|\\-");
			List<Character> splitCharUppercaseList = new LinkedList<>();
			List<Character> splitCharList = new LinkedList<>();

			for(String o : splitArray){
				//System.out.println("inside for loop");
				int length = o.length();
				for (int i = 0; i < length; i++){
					splitCharList.add(o.charAt(i));
					splitCharUppercaseList.add(Character.toUpperCase(o.charAt(i)));
				}
			}
			List<Character> splitCharUppercaseRotated = new LinkedList<>();


			// We will iterate over the character list splitCharList
			// Each iteration will push out a character to check against the list w/ in
			// vanillaAlphabet, It will grab the corresponding key and find the same
			// value inside of rotAlphabet. Finally, it will create a new list
			// with the character obtained from rotAlphabet

			for (char o : splitCharUppercaseList){
				//System.out.println("inside splitCharList");
				//System.out.println(vanillaAlphabet.indexOf(o));
				int index = vanillaAlphabet.indexOf(o);
				if (index > -1){
					char value = rotAlphabet.get(index);
					//System.out.println(rotAlphabet.get(index));
					splitCharUppercaseRotated.add(value);

				} else{
					splitCharUppercaseRotated.add(o);
				}
			}

			// We're going to iterate over the chars in splitCharList
			// we're going to compare the case values of those characters vs the characters
			// in splitCharUppercaseRotated. if the values match, we will keep case
			// and add them to the new list we will create: splitCharRotatedCasefix
			// if they do not match, we will fix them, and add them to the same list

			List<Character> splitCharRotatedCaseFix = new LinkedList<>();
			int v = 0;

			for (char o : splitCharList){
				//System.out.println("o: "+ o);
				if(Character.isUpperCase(o)){
					splitCharRotatedCaseFix.add(splitCharUppercaseRotated.get(v));
				} else{
					splitCharRotatedCaseFix.add(Character.toLowerCase(splitCharUppercaseRotated.get(v)));
				}
				v++;

			}
			/*System.out.println("SCRCF: " + splitCharRotatedCaseFix);
			System.out.println("Rotated: " + splitCharUppercaseRotated);
			System.out.println(rotAlphabet);
			System.out.println(splitCharList);
			System.out.println("Below for loop");
			System.out.println(string);*/

			StringBuilder strbul = new StringBuilder();
			for (char o : splitCharRotatedCaseFix ){
				strbul.append(o);
			}
			String ciphered = strbul.toString();
			System.out.println("ciphered: " + ciphered);

			// iteration over original string
			// each iteration will find the index of " " inside of the original string


			int spaces = string.indexOf(" ");
			int spaceHolder = 0;
			int count = 0;
			long spaceCount = string.chars().filter(ch -> ch == ' ').count();
			List<String> substringList = Arrays.asList(string.split("\\s"));
			int test = 2;
			System.out.println(substringList);
			System.out.println("spaceCount: "	+ spaceCount);

			List<String> lastStringList = new ArrayList<>();
			int zero = 0;

			if (spaces > 0) {
				System.out.println("inside if");
				System.out.println(ciphered);
				System.out.println(string);

				while (zero <= spaceCount) {
					int otherCount = 0;

					for (String o: substringList){
						//System.out.println("length: "+ o.length());
						// Loop through the characters inside arrayList SCRCF
						// append them to string builder until I reach a point where
						// the amount I've looped thru is equal the string "o" length
						// build that string and push it to lastStringList

						StringBuffer lsl = new StringBuffer();
						lsl.setLength(0);
						System.out.println("o:" +o);

						boolean flag = Character.isDigit(o.charAt(0));

						//System.out.println("size: " + splitCharRotatedCaseFix.size());


						//System.out.println("i: " + otherCount);
						//System.out.println("i + length: " + (otherCount+o.length()));
						StringBuffer infinityStone = new StringBuffer();
						if(flag){
							System.out.println("inside flag");
							infinityStone.append(o);
							String numberCipher = infinityStone.toString();
							lastStringList.add(numberCipher);
							System.out.println(lastStringList);
							otherCount += o.length();
							continue;
						}
						System.out.println("oC: " + otherCount);

						try	{
							List<Character> sublist = splitCharRotatedCaseFix.subList(otherCount, otherCount + o.length());
							for(char ch : sublist) {
								infinityStone.append(ch);
							}
							String tripleCipher = infinityStone.toString();
							lastStringList.add(tripleCipher);
							otherCount += o.length();
						/*if ((otherCount+o.length()) > o.length()){
							for (char z : splitCharRotatedCaseFix.subList(otherCount, o.length())) {
								System.out.println("inside if");
								infinityStone.append(z);
								count++;
								if (count == o.length()) {
									break;
								}
								break;
							}
							break;
						}else {
							for (char z : splitCharRotatedCaseFix.subList(otherCount, (otherCount + o.length()))) {
								System.out.println("inside else");
								infinityStone.append(z);
								count++;
								if (count == o.length()) {
									break;
								}
								break;

							}
							String infinityString = infinityStone.toString();
							System.out.println("IS string: "+ infinityStone);
							lastStringList.add(infinityString);
							System.out.println("IS string: "+infinityString);
							otherCount += o.length();
							break;*/
							System.out.println(lastStringList);
							zero++;
						}catch(IndexOutOfBoundsException e){
							List<Character> sublist = splitCharRotatedCaseFix.subList(otherCount, splitCharRotatedCaseFix.size());
							for(char ch : sublist) {
								infinityStone.append(ch);
							}
							String tripleCipher = infinityStone.toString();
							lastStringList.add(tripleCipher);
							otherCount += o.length();
						/*if ((otherCount+o.length()) > o.length()){
							for (char z : splitCharRotatedCaseFix.subList(otherCount, o.length())) {
								System.out.println("inside if");
								infinityStone.append(z);
								count++;
								if (count == o.length()) {
									break;
								}
								break;
							}
							break;
						}else {
							for (char z : splitCharRotatedCaseFix.subList(otherCount, (otherCount + o.length()))) {
								System.out.println("inside else");
								infinityStone.append(z);
								count++;
								if (count == o.length()) {
									break;
								}
								break;

							}
							String infinityString = infinityStone.toString();
							System.out.println("IS string: "+ infinityStone);
							lastStringList.add(infinityString);
							System.out.println("IS string: "+infinityString);
							otherCount += o.length();
							break;*/
							System.out.println(lastStringList);
							zero++;
						}
						/*System.out.println("zero: " + zero);
						System.out.println("spaceCount: " + spaceCount);*/
					}
					String kb = "kb";

					StringBuffer uk = new StringBuffer();
					for (String o : lastStringList){
						uk.append(o);
						uk.append(" ");
					}
					String quadCipher = uk.toString();
					String quadCipher2 = quadCipher.trim();
					return quadCipher2;
				}

				System.out.println(substringList);
				StringBuffer sb = new StringBuffer();
				for (String x : substringList){
					sb.append(x);
					sb.append(" ");
				}
				String omegaCiphered = sb.toString();
				String omegaCiphered2 = omegaCiphered.trim();
				System.out.println("omegaCiphered: " + omegaCiphered2);

				return omegaCiphered2;
			}
			return ciphered;
		}
	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param n
	 * @return
	 */
	public int calculateNthPrime(int n) {
		// TODO Write an implementation for this method declaration
		if (n == 0){throw new IllegalArgumentException();}
		if (n < 2) return 2;
		if (n == 2) return 3;
		int limit, root, count = 1;
		limit = (int)(n*(Math.log(n) + Math.log(Math.log(n)))) + 3;
		root = (int)Math.sqrt(limit) + 1;
		limit = (limit-1)/2;
		root = root/2 - 1;
		boolean[] sieve = new boolean[limit];
		for(int i = 0; i < root; ++i) {
			if (!sieve[i]) {
				++count;
				for(int j = 2*i*(i+3)+3, p = 2*i+3; j < limit; j += p) {
					sieve[j] = true;
				}
			}
		}
		int p;
		for(p = root; count < n; ++p) {
			if (!sieve[p]) {
				++count;
			}
		}
		return 2*p+1;
	}

	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {



		AtbashCipher(){

		}

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			// TODO Write an implementation for this method declaration
			String plain = "abcdefghijklmnopqrstuvwxyz";
			String cipher = "zyxwvutsrqponmlkjihgfedcba";
			char[] cipherArray = cipher.toCharArray();
			//System.out.println(Arrays.toString(plainArray));
			//System.out.println(Arrays.toString(cipherArray));


			String lowercaseString = string.toLowerCase(Locale.ROOT);
			String noSpace = lowercaseString.replaceAll("\\s","");
			String noCommaNoSpace = noSpace.replaceAll(",", "");
			String noPeriodNoCommaNoSpace = noCommaNoSpace.replaceAll("[.]","");
			char[] stringArray = noPeriodNoCommaNoSpace.toCharArray();
			List<Character> cipherArrayList = new ArrayList<>();

			System.out.println("before first for loop");
			for (char o : stringArray){
				System.out.println(o);
				int plainInt = plain.indexOf(o);

				if (Character.isDigit(o) || plainInt == -1){
					cipherArrayList.add(o);
				}else{
					char cipherChar = cipherArray[plainInt];
					cipherArrayList.add(cipherChar);
				}


			}
			System.out.println(cipherArrayList);
			StringBuilder sb = new StringBuilder();

			for(char o : cipherArrayList){
				sb.append(o);
			}
			String encodedString = sb.toString();
			List<String> encodedList = new ArrayList<String>();
			int i = 0;
			while(i < encodedString.length()){
				encodedList.add(encodedString.substring(i, Math.min(i + 5, encodedString.length())));
				i += 5;
			}
			System.out.println("encoded with string: " + encodedList);
			StringBuilder fs = new StringBuilder();
			for (String o : encodedList){
				fs.append(o);
				fs.append(" ");
			}
			String encodedFs = fs.toString();
			String encodedFsAndTrim = encodedFs.trim();
			System.out.println(encodedFsAndTrim);
			//String encodedAndTrim = encodedString.trim();

			//System.out.println(encodedAndTrim);

			return encodedFsAndTrim;
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			// TODO Write an implementation for this method declaration
			String plain = "abcdefghijklmnopqrstuvwxyz";
			String cipher = "zyxwvutsrqponmlkjihgfedcba";
			char[] plainArray = plain.toCharArray();
			String noSpaces = string.replaceAll("\\s", "");

			List<Character> plainArrayList = new ArrayList<>();
			char[] stringArray = noSpaces.toCharArray();

			for(char o : stringArray){
				int cipherInt = cipher.indexOf(o);
				if (Character.isDigit(o) || cipherInt == -1){
					plainArrayList.add(o);
				}else{
					char cipherChar = plainArray[cipherInt];
					plainArrayList.add(cipherChar);
				}
			}
			StringBuilder sb = new StringBuilder();
			for (char o : plainArrayList){
				sb.append(o);
			}
			String plainString = sb.toString();
			System.out.println(plainString);

			return plainString;
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		// TODO Write an implementation for this method declaration
		String cleanString = string.replaceAll("-","");
		System.out.println(cleanString);
		List<Integer> cleanIntArrayList = new ArrayList<>();
		char[] cleanStringCharArray = cleanString.toCharArray();
		int i = 10;
		int z = 0;
		for(char o : cleanStringCharArray){

			if (o == 'X'){
				System.out.println("inside if");
				z = 10;
			}else if (!Character.isDigit(o)){
				return false;
			}else {
				z = Integer.parseInt(String.valueOf(o));
			}
			System.out.println(o);
			System.out.println(i);

			z *= i;
			i--;
			cleanIntArrayList.add(z);
		}

		System.out.println(cleanIntArrayList);
		z = 0;
		for (int o : cleanIntArrayList){
			z+= o;
		}

		if (z % 11 == 0){
			return true;
		}else{
			return false;
		}



	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		// TODO Write an implementation for this method declaration


		String noSpaces = string.replaceAll("\\s", "");
		char[] arrayStringChars = noSpaces.toCharArray();
		int i = 0;
		System.out.println(noSpaces.length());

		if (noSpaces.length() >= 26) {
			for (char alphabet = 'a'; alphabet <='z'; alphabet ++){
				System.out.println("inside for: " + alphabet);

				for (char o : arrayStringChars){
					System.out.println("Inside o: " + o);
					if (o == alphabet){
						i++;
						break;
					}
				}
				System.out.println("outside for");
			}
			return i >= 26;
		}


		return false;
	}

	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		// TODO Write an implementation for this method declaration
		// Take the temporal that is given to us and find the date and time that is
		// a gigasecond past that moment.
		// time requirements to return: Year, Month, Day, Hour, Minute, Second

		long gigaSecond = 1000000000;
		if (given.getClass() == LocalDate.class){
			LocalDateTime convert = ((LocalDate) given).atStartOfDay();
			//System.out.println("inside if");
			LocalDateTime gigaLife = convert.plus(gigaSecond, ChronoUnit.SECONDS);
			return gigaLife;
		} else{
			LocalDateTime convert = LocalDateTime.from(given);
			LocalDateTime gigaLife = convert.plus(gigaSecond, ChronoUnit.SECONDS);
			return gigaLife;
		}

	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		// TODO Write an implementation for this method declaration
		List<Integer> multipleList = new ArrayList<>();

		for (int o : set){
			int z = o;
			while (z < i){
				if (multipleList.contains(z)){
					z+= o;
					continue;
				}else{
					multipleList.add(z);
					z+= o;
				}
			}
		}
		System.out.println(multipleList);
		int z = 0;
		for (int o : multipleList){
			z+=o;
		}
		return z;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		// TODO Write an implementation for this method declaration
		String stripped = string.replaceAll("\\s","");
		List<Integer> arrayList = new ArrayList<>();

		int iteration = 1;
		for (int i = stripped.length() - 1; i > -1;i--){
			if (!Character.isDigit(stripped.charAt(i))){
				return false;
			} else if (iteration % 2 == 0){
				int sc = Character.getNumericValue(stripped.charAt(i));
				sc *= 2;
				if (sc > 9){
					sc -= 9;

					System.out.println("inside else if: if: " + sc);
					arrayList.add(sc);
					iteration++;
					continue;
				}

				System.out.println("inside else if"+ sc);
				arrayList.add(sc);
			}else{
				int sc = Character.getNumericValue(stripped.charAt(i));

				System.out.println("inside else: " + sc);
				arrayList.add(sc);
			}
			iteration++;
		}
		int adder = 0;
		for(int o : arrayList){
			adder += o;
		}
		if (adder % 10 == 0){
			return true;
		}
		System.out.println(arrayList);

		return false;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		// TODO Write an implementation for this method declaration
		List<String> mathList = new ArrayList<>();
		String[] mathStrings = new String[]{"plus", "minus","multiplied","divided"};

		for (String o : mathStrings){
			mathList.add(o);
		}
		System.out.println(mathList);
		String stringTrim = string.replace("?","");
		String[] stringList = stringTrim.split("\\s");
		System.out.println(Arrays.toString(stringList));

		List<Integer> solveList = new ArrayList<>();

		for (String o : stringList){
			int mathSwitch = mathList.indexOf(o);
			switch (mathSwitch){
				case -1:
					continue;

				case 0: // addition
					for (String x : stringList){
						if(x.matches("-?\\d+")){
							solveList.add(Integer.parseInt(x));
						}
					}
					System.out.println(solveList);
					int y = 0;
					int r = 0;
					for (int z : solveList){
						System.out.println(z);
						if (r == 0){
							y = z;
						}else{
							y+=z;
						}
						r++;
					}
					return y;

				case 1: // subtraction
					for (String x : stringList){
						if(x.matches("-?\\d+")){
							solveList.add(Integer.parseInt(x));
						}
					}
					System.out.println(solveList);
					y = 0;
					r = 0;
					for (int z : solveList){
						System.out.println(z);
						if (r == 0){
							y = z;
						}else{
							y-=z;
						}
						r++;
					}
					return y;
				case 2: // multiplication
					for (String x : stringList){
						if(x.matches("-?\\d+")){
							solveList.add(Integer.parseInt(x));
						}
					}
					System.out.println(solveList);
					y = 0;
					r = 0;
					for (int z : solveList){
						System.out.println(z);
						if (r == 0){
							y = z;
						}else{
							y*=z;
						}
						r++;
					}
					return y;
				case 3: // division
					for (String x : stringList){
						if(x.matches("-?\\d+")){
							solveList.add(Integer.parseInt(x));
						}
					}
					System.out.println(solveList);
					y = 0;
					r = 0;
					for (int z : solveList){
						System.out.println(z);
						if (r == 0){
							y = z;
						}else{
							y/=z;
						}
						r++;
					}
					return y;
			}
		}
		return 0;
	}

}
